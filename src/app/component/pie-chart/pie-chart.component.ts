import { Component, OnInit } from '@angular/core';
import { DbServices } from '../../services/db.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  // Pie Chart Data
  public pieChartLabels: string[] = ['Red', 'Orange', 'Yellow', 'Green', 'Blue' ];
  public pieChartData: number[] = [63, 102, 87, 99, 200, 18, 45, 98, 108, 60 ];
  public pieChartType = 'pie';
  public pieChartColors: any[] = [
    { backgroundColor: ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)',
                        'rgb(115,64,148)', 'rgb(153, 102, 255)', 'rgb(255,67,181)', 'rgb(155,199,94)', 'rgb(94, 189, 235)',
                        'rgb(34, 62, 35)'
                       ]
    }
  ];
  constructor(public dbService: DbServices) { }

  ngOnInit() {
    this.dbService.getDetails('/perfomence', '', {}).subscribe((list) => {
      console.log(list);
    }, (err) => {
      console.log(err);
    });
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
