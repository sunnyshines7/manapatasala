import { Component, OnInit } from '@angular/core';
import { DbServices } from '../../services/db.service';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  // Line Chart Data
  public lineChartData: Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40, 17, 0, 0, 30, 0], label: '2018 Results'},
    {data: [28, 48, 40, 19, 86, 27, 90, 0, 22, 55, 0, 11], label: '2017 Results'}
  ];
  public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
                                         'August', 'September', 'October', 'November', 'December'];
  public lineChartType = 'line';
  public lineChartLegend = true;
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'transparent',
      borderColor: 'rgb(255, 99, 132)'
    },
    { // dark grey
      backgroundColor: 'transparent',
      borderColor: 'rgb(54, 162, 235)'
    }
  ];

  constructor(public dbService: DbServices) { }

  ngOnInit() {
    this.dbService.getDetails('/attendence', '', {}).subscribe((list) => {
      console.log(list);
    }, (err) => {
      console.log(err);
    });
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

}
