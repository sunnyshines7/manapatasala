import { Component, OnInit } from '@angular/core';
import { DbServices } from '../../services/db.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(public dbService: DbServices) { }

  ngOnInit() {
    this.dbService.getDetails('/dashboard', '', {}).subscribe((resp) => {
      console.log(resp);
    }, (err) => {
      console.log(err);
    });
  }


}
