import { Component, OnInit, ViewChild } from '@angular/core';

import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';

@Component({
  selector: 'app-event-management',
  templateUrl: './event-management.component.html',
  styleUrls: ['./event-management.component.css']
})
export class EventManagementComponent implements OnInit {
  calendarOptions: Options;
  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;
  constructor() { }

  ngOnInit() {
    this.calendarOptions = {
      editable: true,
      eventLimit: false,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
      },
      selectable: true,
      events: [
        {
          title  : 'event1',
          start  : '2018-06-01'
        },
        {
          title  : 'event2',
          start  : '2018-06-05',
          end    : '2018-06-07'
        },
        {
          title  : 'event3',
          start  : '2018-06-09T12:30:00',
          allDay : false // will make the time show
        }
      ]
    };
  }

  openModal(id: string) {
    // $('#addNewEvent').modal('open');
  }

  closeModal(id: string) {
    // $('#addNewEvent').modal('open');
  }

}
