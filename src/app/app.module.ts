import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DataTablesModule } from 'angular-datatables';
import { FullCalendarModule } from 'ng-fullcalendar';
import { ChartsModule } from 'ng4-charts/ng4-charts';

import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { Routes, RouterModule } from '@angular/router';
import { NotfoundComponent } from './component/notfound/notfound.component';
import { AllFeesComponent } from './component/all-fees/all-fees.component';
import { HttpModule } from '@angular/http';
import { EventManagementComponent } from './component/event-management/event-management.component';
import { PieChartComponent } from './component/pie-chart/pie-chart.component';
import { LineChartComponent } from './component/line-chart/line-chart.component';
import { DbServices } from './services/db.service';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';

const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'events', component: EventManagementComponent},
  {path: 'fees', component: AllFeesComponent},
  {path: '**', component: NotfoundComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    SidebarComponent,
    NotfoundComponent,
    AllFeesComponent,
    EventManagementComponent,
    PieChartComponent,
    LineChartComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    DataTablesModule,
    HttpModule,
    RouterModule.forRoot(routes),
    FullCalendarModule,
    ChartsModule
  ],
  providers: [DbServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
