import { TestBed, inject } from '@angular/core/testing';
import { DbServices } from './db.service';


describe('DbServices', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DbServices]
    });
  });

  it('should be created', inject([DbServices], (service: DbServices) => {
    expect(service).toBeTruthy();
  }));
});
