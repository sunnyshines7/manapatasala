import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

@Injectable()
export class DbServices {

  headers: Headers;
  baseurl = '';

  constructor(public http: Http) {
    this.headers = new Headers();
    this.headers.append('X-Parse-Application-Id', 'asf');
    this.headers.append('Content-Type', 'application/json');

    this.baseurl = 'https://localhost:8080/';
  }

  getDetails(url, params, data) {
    let finalurl = url;
    finalurl = params ? finalurl + params : finalurl;
    finalurl = data ? finalurl + JSON.stringify(data) : finalurl;
    return this.http.get(this.baseurl + finalurl, {headers: this.headers})
            .map((resp: Response) => {
                return resp.json();
            });
  }
}
